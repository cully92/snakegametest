public void place() {
   Cell cell = board.getCell(0, 2);
   cell.setType(CellType.HEAD);
   
   Cell body = board.getCell(0, 1);
   body.setType(CellType.BODY);
   
   Cell tail = board.getCell(0, 0);
   tail.setType(CellType.TAIL);
}