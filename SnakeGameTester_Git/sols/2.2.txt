The full code for the Direction enum is shown below. It should be in a file called 'Direction.java' in the package 'com.snake'

package com.snake;

/**
 * Enumerated type holding different directions, and NONE
 */
public enum Direction {
   NORTH, EAST, SOUTH, WEST, NONE
}