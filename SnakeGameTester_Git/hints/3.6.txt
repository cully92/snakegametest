Have you created an int field called score in the Snake class?
   e.g private int score = 0;
   
The pseudocode for checkColissions is as follows:
public void checkCollision(int row, int col) throws GameOverException {
	// Get the cell held at (row, col)
	
	// if cell.getType == CellType.FOOD
	//   Update the score field
	//   Update the label on the ScoreBar
	//   Place more food
	// else if cell.getType != CellType.EMPTY
	//   throw a GameOverException
}

Have you called checkColissions from your moveCell() method?
Make sure you read the instructions just below the box for this exercise as well, as you will get some warnings because of the GameOverException. The worksheet details how to fix this.