Make sure the Direction.java is an enum rather than a class. You can tell this if it has the following:
	public enum Direction {

You also want to ensure it is in the com.snake.ui package
Enum values are normally all uppercase, and different values of the enum should be separated by commas.