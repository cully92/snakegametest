   private Cell moveCell(Cell cell, CellType type) {
      // Get the row, column and direction of the old Cell
      int row = cell.getRow();
      int col = cell.getCol();
      Direction dir = cell.getDirection();
      
      // Reset the current Cell to be empty
      cell.setType(CellType.EMPTY);
      cell.setDirection(Direction.NONE);

      switch (dir) {
         case NORTH:
            row--;
            break;
         case EAST:
            col++;
            break;
         case WEST:
            col--;
            break;
         case SOUTH:
            row++;
            break;
      }

	  // Setup the new Cell to the given type
      Cell newCell = board.getCell(row, col);
      newCell.setType(type);
      newCell.setDirection(dir);

      return newCell;
   }