package snakegametest.socket;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Protocol class for dealing with processing input from the client
 *
 * @author Sean
 */
public class SnakeResultProtocol {

   private boolean reportStarted = false;
   private String user = "";
   private List<String> reportLines = new ArrayList<String>();

   public String processInput(String input) {
      
      // Start connection if null passed in
      if (input == null) {
         return "Connection initiated";

         // Log username (the next message)
      } else if (input.startsWith("UN")) {
         reportStarted = true;
         this.user = input.replace("UN:", "");
         return "User ID: " + user;

      } else if (input.equals("REPORTEND")) {
         reportStarted = false;
         System.out.println("Report compiled for: " + user);
         System.out.println("Accepted " + reportLines.size() + " lines");
         
         writeReportToFile("reports/report."+user+".txt", reportLines);

         // This causes the client to end the connection
         return "Bye.";

         
         // Client will now start to send the report, which we accumulate here.
         // This stops when the client sends "REPORTEND"
      } else if (reportStarted) {
         reportLines.add(input);
         return "Input received";

         // The client has finished sending the report
      }

      return "Input not recognised!";
   }

   private void writeReportToFile(String fileName, List<String> reportLines) {
      BufferedWriter writer = null;
      try {
         File reportFile = new File(fileName);
         if (!reportFile.exists()) {
            reportFile.createNewFile();
         }

         writer = new BufferedWriter(new FileWriter(reportFile));
         for (String result : reportLines) {
               writer.write(result + "\n");
         }

      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try {
            writer.close();
         } catch (IOException e) {}
      }
   }

}
