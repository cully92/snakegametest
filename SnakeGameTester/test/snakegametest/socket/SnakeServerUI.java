package snakegametest.socket;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import snakegametest.runner.Report;

/**
 *
 */
public class SnakeServerUI extends JFrame {

   private JPanel panel;
   private JTextArea reportDisplayArea;
   private final String SPACING = "     ";
   private JList listBox;
   private int portNumber;
   private SnakeServer serverThread;
   private JButton startServerBtn;
   private boolean serverStarted = false;
   private static JLabel statusLabel = new JLabel();
   private JTextField scoreText;
   private String INITIAL_TEXT = "Select a report to display here.";

   public SnakeServerUI () {
      setupUI();
      addPanel();

      getPortNumber();
      serverThread = new SnakeServer(portNumber);
   }

   private void setupUI() {
      // Here we set the title and size of the window
      setTitle("Snake Report Collector");
      setSize(450, 300);
      setResizable(true);

      // This tells the program to exit when the red 'X' in the top right
      // corner is clicked
      setDefaultCloseOperation(EXIT_ON_CLOSE);

      // This makes the window appear in the middle, and stops you from
      // resizing the window
      setLocationRelativeTo(null);
      setResizable(true);

      setFocusable(true);
   }

      /**
    * Add the list of failed tests and the hint area
    */
   private void addPanel() {
      panel = new JPanel(new BorderLayout(20, 10));
      panel.setBorder(new EmptyBorder(10, 10, 10, 10));
      getContentPane().add(panel, BorderLayout.CENTER);

      setupReportsList();

      JPanel reportPanel = new JPanel(new BorderLayout(0, 20));
      panel.add(reportPanel, BorderLayout.EAST);

      reportDisplayArea = new JTextArea(30, 70);
      // Set the text to the initial failure message
      reportDisplayArea.setText(INITIAL_TEXT);
      reportDisplayArea.setEditable(false);
      reportPanel.add(new JScrollPane(reportDisplayArea), BorderLayout.CENTER);

      JPanel buttonPanel = getButtonPanel();
      panel.add(buttonPanel, BorderLayout.SOUTH);

      pack();
   }

   private JPanel getButtonPanel() {
      JPanel buttonPanel = new JPanel();

      JButton refreshReports = new JButton("Refresh reports");
      refreshReports.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            listBox.setListData(getUserNames());
            scoreText.setText("  -  ");
            reportDisplayArea.setText(INITIAL_TEXT);
         }
      });

      startServerBtn = new JButton("Start Server");
      startServerBtn.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            if (!serverStarted) {
               serverThread.start();
               serverStarted = true;
               startServerBtn.setText("Server started");
               startServerBtn.setEnabled(false);
            }
         }
      });

      statusLabel.setText("Server has not been started.");

      buttonPanel.add(refreshReports, BorderLayout.WEST);
      buttonPanel.add(startServerBtn, BorderLayout.EAST);
      buttonPanel.add(statusLabel, BorderLayout.SOUTH);

      return buttonPanel;
   }

   public static void setStatusLabel(String status) {
      statusLabel.setText(status);
   }

   /**
    * Set up the listbox showing test failures, and add a selection listener
    * so that we can initially display the error message
    */
   private void setupReportsList() {

      listBox = new JList(getUserNames());
      listBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

      JPanel leftPanel = new JPanel(new BorderLayout());
      panel.add(leftPanel, BorderLayout.WEST);

      JScrollPane scrollableListBox = new JScrollPane(listBox);
      scrollableListBox.setPreferredSize(new Dimension(100, 200));
      leftPanel.add(scrollableListBox, BorderLayout.PAGE_START);
      listBox.addListSelectionListener(new ListSelectionListener() {

         @Override
         public void valueChanged(ListSelectionEvent e) {
            JList source = (JList) e.getSource();
            String user = (String) source.getSelectedValue();

            if (user != null)
               reportDisplayArea.setText(getReportForUser(user.replace(SPACING, "")));

            scoreText.setText(getUserScore(user));
         }
      });

      JPanel scorePanel = getScorePanel();
      leftPanel.add(scorePanel, BorderLayout.PAGE_END);
   }

   private JPanel getScorePanel() {
      
      JPanel scorePanel = new JPanel();
      
      JLabel scoreLabel = new JLabel("Score: ");
      scorePanel.add(scoreLabel);

      scoreText = new JTextField(6);
      scoreText.setText("  -  ");
      scoreText.setEnabled(false);
      scorePanel.add(scoreText);

      return scorePanel;
   }

   private String getUserScore(String user) {
      if (user == null)
         return "  -  ";
      
      user = user.replaceAll(" ", "");
      Report report = new Report("reports/report."+user+".txt");
      return "" + report.calculateScore();
   }

   private String getReportForUser(String userName) {
      List<String> reportLines = getLinesFromFile("reports/report."+userName + ".txt");
      String report = "";

      for (String line : reportLines) {
         report += line + "\n";
      }

      return report;
   }

   public String[] getUserNames() {

      File reportsFolder = new File("reports/");
      File[] files = reportsFolder.listFiles();

      String[] userNames;
      if (files == null) {
    	  return new String[0];
      } else {
    	  userNames = new String[files.length];
      }

      if (reportsFolder.isDirectory()) {
         for (int i = 0; i < userNames.length; i++) {
            // Report file name in format report.<username>.txt
            String[] reportParts = files[i].getName().split("\\.");
            userNames[i] = reportParts[1] + SPACING;
         }
      }

      return userNames;
   }

   public static void main (String[] args) {
      SwingUtilities.invokeLater(new Runnable() {

         @Override
         public void run() {
            SnakeServerUI ui = new SnakeServerUI();
            ui.setVisible(true);
         }
      });
   }

   private void getPortNumber() {
      List<String> portDetails = getLinesFromFile("conn.txt");
      if (portDetails.isEmpty())
         portNumber = 7;
      else
         portNumber = Integer.parseInt(portDetails.get(1));
   }

   /**
    * Return the lines of the given file as Strings
    */
   public static List<String> getLinesFromFile(String fileName) {
      File file = new File(fileName);
      List<String> lines = new ArrayList<String>();
      if (!file.exists()) {
         return lines;
      }

      BufferedReader reader = null;
      try {
         reader = new BufferedReader(new FileReader(file));

         String line;
         while ((line = reader.readLine()) != null) {
            lines.add(line);
         }
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (reader != null) reader.close();
         } catch (IOException e) {}
      }
      return lines;
   }
}
