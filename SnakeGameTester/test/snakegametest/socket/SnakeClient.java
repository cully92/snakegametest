package snakegametest.socket;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import snakegametest.runner.Report;

/**
 * Client class, responsible for sending the report to the server. Essentially a class for testing / learning about
 * Sockets / client-server implementation
 *
 * Initial implementation based on Oracle tutorial:
 * http://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
 *
 * @author Sean
 */
public class SnakeClient {

   public static void main(String[] args) {
      Report report = new Report();
      report.sendToServer();
   }

}
