package snakegametest.socket;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 * Client class, responsible for sending the report to the server. Essentially a
 * class for testing / learning about Sockets / client-server implementation
 *
 * Initial implementation based on Oracle tutorial:
 * http://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
 *
 * @author Sean
 */
public class SnakeTestClient {

   private static String ERROR_MSG = "Error connecting to the server. Please check the SnakeServer is running.";
   private static String hostName = "localhost";
   private static int port = 7;

   public static void main(String[] args) {

      try {

         //Connect to the server
         getConnectionDetails();
         Socket socket = new Socket(hostName, port);
         System.out.println("Connected to the server!");
         System.out.println("------------------------");
         System.out.println("Socket address: " + socket.getLocalSocketAddress());
         System.out.println("IP: " + socket.getInetAddress());
         System.out.println("Port: " + socket.getPort());
         System.out.println("------------------------");
         System.out.println("Success, exiting test client");

      } catch (UnknownHostException ex) {
         System.out.println(ERROR_MSG);
         System.out.println("UnknownHostException: " + ex.getMessage());
      } catch (IOException ex) {
         System.out.println(ERROR_MSG);
         ex.printStackTrace();
      }
   }

   /**
    * Read the connection details from a file called "conn.txt", and set the
    * port and hostname details
    */
   private static void getConnectionDetails() {
      List<String> connLines = getLinesFromFile("conn.txt");
      if (connLines.size() >= 2) {
         hostName = connLines.get(0);
         port = Integer.parseInt(connLines.get(1));
      }
   }

   /**
    * Return the lines of the given file as Strings
    */
   private static List<String> getLinesFromFile(String fileName) {
      File file = new File(fileName);
      List<String> lines = new ArrayList<String>();
      if (!file.exists()) {
         return lines;
      }

      BufferedReader reader = null;
      try {
         reader = new BufferedReader(new FileReader(file));

         String line;
         while ((line = reader.readLine()) != null) {
            lines.add(line);
         }
      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (reader != null) reader.close();
         } catch (IOException e) {}
      }
      return lines;
   }
}
