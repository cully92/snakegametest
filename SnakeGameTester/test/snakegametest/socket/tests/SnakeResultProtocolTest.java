package snakegametest.socket.tests;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import snakegametest.runner.SnakeTestResult;
import snakegametest.socket.SnakeResultProtocol;
import static org.junit.Assert.*;

/**
 *
 * @author Sean
 */
public class SnakeResultProtocolTest {

   private SnakeResultProtocol protocol;
   private List<String> input;
   private static String user = "TestUser";

   @Before
   public void before() {
      protocol = new SnakeResultProtocol();

      input = new ArrayList<String>();
      input.add(null);
      input.add("UN:" + user);
      input.add("Exercise 1.1	Run: 9	Succeeded: 9	Failed: 0	Regressed: 0	H0S0	testTitleChanged");
      input.add("Exercise 1.2	Run: 9	Succeeded: 9	Failed: 0	Regressed: 0	H0S0	testGridCorrectSize");
      input.add("REPORTEND");
   }

   @AfterClass
   public static void afterClass() {
      File file = new File("reports/report." + user + ".txt");

      if (file.exists())
         file.deleteOnExit();
   }

   @Test
   public void testSnakeResultProtocolValidInitiation() {
      // 'null' initiates connection
      String result = protocol.processInput(input.get(0));
      String expected = "Connection initiated";
      assertTrue("SnakeResultProtocol processes initial input correctly", result.equals(expected));
   }

   @Test
   public void testSnakeResultProtocolInvalidInitiation() {
      String result = protocol.processInput("invalid");
      String expected = "Input not recognised!";
      assertTrue("Protocol processes invalid initial input correctly", result.equals(expected));
   }

   @Test
   public void testProtocolAcceptsUserName() {
      String result = protocol.processInput(input.get(1));
      String expected = "User ID: " + user;
      assertTrue("Protocol accepts username", result.equals(expected));
   }

   @Test
   public void testProtocolAcceptsSingleReportLine() {
      // Send username to initiate report starting
      protocol.processInput(input.get(1));

      String result = protocol.processInput("ReportLine0");
      String expected = "Input received";
      assertTrue("Protocol accepts a single report line", result.equals(expected));
   }

   @Test
   public void testProtocolAcceptsMultipleReportLines() {
      // Send username to initiate report starting
      protocol.processInput(input.get(1));

      String expected = "Input received";
      for (int i = 0; i < 4; i++) {
         String result = protocol.processInput("ReportLine0");
         assertTrue("Protocol accepts multiple report lines", result.equals(expected));
      }
   }

   @Test
   public void testProtocolRejectsReportIfSessionNotInitiated() {
      String expected = "Input not recognised!";
      String result = protocol.processInput("ReportLine1");

      assertTrue("Protocol doesn't accept report until username sent", result.equals(expected));
   }

   @Test
   public void testProtocolClosesSessionAfterReceivingReport() {
      String expected = "Bye.";
      String finalResult = "";

      for (String inputLine : input) {
         finalResult = protocol.processInput(inputLine);
      }

      assertTrue("Protocol exits correctly after receiving report", finalResult.equals(expected));
   }

   @Test
   public void testReportWrittenToFile() {
      for (String inputLine : input) {
         protocol.processInput(inputLine);
      }

      File file = new File("reports/report." + user + ".txt");
      assertTrue("File written after report received", file.exists());
   }
}
