package snakegametest.socket;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Server class allowing multiple client connections. Once a connection
 * is received, it spawns a new ServerThread to deal with the connection.
 */
public class SnakeServer extends Thread {

   private static int portNumber;
   private static ServerSocket server;
   
   public SnakeServer(int portNumber) {
      this.portNumber = portNumber;
   }

   @Override
   public void run() {

      try {
         server = new ServerSocket(portNumber);
         System.out.println("Waiting for client connections");
         System.out.println("Server details:");
         System.out.println("-------------------------");
         System.out.println("Local Socket Address: " + server.getLocalSocketAddress());
         System.out.println("IP: " + server.getInetAddress());
         System.out.println("Port: " + server.getLocalPort());
         SnakeServerUI.setStatusLabel("Server started! Waiting for connections...");

         // Create the results folder to store results
         File reportsFolder = new File("reports/");
         if (!reportsFolder.exists())
            reportsFolder.mkdir();


         int counter = 0;
         // Loop forever waiting for client connections, until program is closed
         // by user
         while (true) {
            Socket clientSocket = server.accept();

            counter++;
            String status = "Client " + counter + " connection accepted";
            SnakeServerUI.setStatusLabel(status);
            System.out.println(status + clientSocket.toString());

            // Spawn a new thread to deal with the client
            ServerThread thread = new ServerThread(clientSocket);
            thread.start();
         }
      } catch (BindException ex) {
         String msg = "Server already running on this port. Exiting";
         System.out.println(msg);
         SnakeServerUI.setStatusLabel(msg);
      } catch (IOException ex) {
         Logger.getLogger(SnakeServer.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
}
