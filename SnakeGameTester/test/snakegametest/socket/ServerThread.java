package snakegametest.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A special Thread subclass that deals with user input through the
 * SnakeResultProtocol.
 *
 * @author Sean
 */
public class ServerThread extends Thread {

   private final Socket clientSocket;
   private PrintWriter out = null;
   private BufferedReader in = null;

   public ServerThread(Socket clientSocket) {
      this.clientSocket = clientSocket;
   }

   @Override
   public void run() {
      System.out.println("Server thread spawned");
      
      try {
         out = new PrintWriter(clientSocket.getOutputStream(), true);
         in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

         // Protocol is responsible for handling client input
         SnakeResultProtocol protocol = new SnakeResultProtocol();

         // Initiate the connection to the client by passing null to process input
         String inputLine, outputLine;
         outputLine = protocol.processInput(null);
         out.println(outputLine);
         
         // Communicate through protocols processInput method
         handleLiveConnection(protocol);
         
      } catch (IOException ex) {
         System.out.println("Error occurred: " + ex.getMessage());
         System.out.println("Closing connection");
         out.println("Bye.");
      } finally {
         out.close();
      }
   }

   private void handleLiveConnection(SnakeResultProtocol protocol) throws IOException {
      String inputLine, outputLine;
      while ((inputLine = in.readLine()) != null) {
            outputLine = protocol.processInput(inputLine);
            out.println(outputLine);
            if (outputLine.equals("Bye.")) {
               break;
            }
         }
   }
}
