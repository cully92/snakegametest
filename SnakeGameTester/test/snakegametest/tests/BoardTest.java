package snakegametest.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static snakegametest.tests.TestUtils.checkMethodExists;

import org.junit.Before;
import org.junit.Test;

import snakegametest.runner.Task;

import com.snake.Board;
import com.snake.Cell;
import com.snake.CellType;
import com.snake.templates.AbstractBoard;
import com.snake.templates.AbstractCell;
import com.snake.templates.AbstractSnake;
import com.snake.ui.ScoreBar;
import java.awt.event.ActionListener;
import javax.swing.JLabel;
import javax.swing.Timer;

public class BoardTest {

   private String CLASS_NAME = "Board";
   private final int CELL_SIZE = 15;
   private final int COLS = 40;
   private final int ROWS = 20;
   private AbstractBoard board;
   private AbstractSnake snake;

   @Before
   public void before() {
      board = new Board(CELL_SIZE, ROWS, COLS);
      snake = board.getSnake();
   }

   @Task(1.2)
   @Test
   public void testGridCorrectSize() {

      Object value = TestUtils.getFieldValue("grid", board);
      checkNull(value, "Grid");
      Cell[][] grid = (Cell[][]) value;

      assertEquals(ROWS, grid.length);
      assertEquals(COLS, grid[0].length);
   }

   @Task(1.3)
   @Test
   public void testGridPopulatedWithCells() {

      Object value = TestUtils.getFieldValue("grid", board);
      checkNull(value, "Grid");
      Cell[][] grid = (Cell[][]) value;

      Cell cell;
      for (int i = 0; i < ROWS; i++) {
         for (int j = 0; j < COLS; j++) {

            cell = grid[i][j];
            checkNull(cell, "Cell");

            if (!(cell instanceof AbstractCell)) {
               fail("Index " + i + "," + j + " was not a Cell object");
            }
         }
      }
   }

   @Task(1.41)
   @Test
   public void testGetCellReturnsCell() {
      checkMethodExists(CLASS_NAME, "getCell", int.class, int.class);
      AbstractCell cell = board.getCell(0, 0);
      assertTrue("getCell doesn't return a Cell object", cell instanceof AbstractCell);
   }

   @Task(1.72)
   @Test
   public void testSnakeField() {
      assertNotNull("Snake field is null, getSnake() may not have been implemented.", snake);
      TestUtils.testObjectField("snake", snake, board, false);
   }

   @Task(1.73)
   @Test
   public void testGetSnakeReturnsSnake() {
      AbstractSnake snake = board.getSnake();
      assertNotNull("getSnake returns null", snake);
   }

   @Task(2.5)
   @Test
   public void testTimerField() {
      // First check it exists
      TestUtils.checkFieldExists("timer", board);

      // Call start, to set up the timer object
      board.start();
      
      // Then check the value
      Object timer = TestUtils.getFieldValue("timer", board);
      if (!(timer instanceof Timer)) {
         fail("timer field does not contain a Timer object");
      }
   }

   @Task(2.5)
   @Test
   public void testTimerListenerField() {
      TestUtils.checkFieldExists("timerListener", board);

      board.start();
      
      Object listener = TestUtils.getFieldValue("timerListener", board);
      if (!(listener instanceof ActionListener)) {
         fail("timerListener field does not contain a TimerListener object");
      }
   }

   @Task(2.5)
   @Test
   public void testCallingStartStartsTimer() {
      board.start();

      Timer timer = (Timer) TestUtils.getFieldValue("timer", board);
      assertTrue("Calling start() doesn't start the timer", timer.isRunning());
   }

   @Task(2.5)
   @Test
   public void testCallingStartChangesStatusMessage() {
      board.start();
      JLabel label = (JLabel) TestUtils.getFieldValue("statusLabel", ScoreBar.getInstance());

      String statusLabel = label.getText();
      if (statusLabel.equals("Press Enter to start")) {
         fail("Status label not changed in start() method");
      }
   }

   @Task(2.6)
   @Test
   public void testSetTimerMethodTrue() {
      board.start();
      Timer timer = (Timer) TestUtils.getFieldValue("timer", board);

      timer.stop();
      board.setTimer(true);

      timer = (Timer) TestUtils.getFieldValue("timer", board);
      assertTrue("Passing true to setTimer starts the timer", timer.isRunning());
   }

   @Task(2.6)
   @Test
   public void testSetTimerMethodFalse() {
      board.start();
      Timer timer = (Timer) TestUtils.getFieldValue("timer", board);
      timer.stop();
      board.setTimer(false);

      timer = (Timer) TestUtils.getFieldValue("timer", board);
      assertFalse("Passing false to setTimer stops the timer", timer.isRunning());
   }

   @Task(3.5)
   @Test
   public void testPlaceFoodMethodPlacesFood() {
      checkMethodExists(CLASS_NAME, "placeFood");
      ((Board) board).placeFood();

      for (int row = 0; row < ROWS; row++) {
         for (int col = 0; col < COLS; col++) {
            if (board.getCell(row, col).getType().equals(CellType.FOOD)) {
               return;
            }
         }
      }

      fail("No FOOD cell found");
   }

   @Task(3.5)
   @Test
   public void testCallingStartPlacesFood() {
      checkMethodExists(CLASS_NAME, "placeFood");
      board.start();

      for (int row = 0; row < ROWS; row++) {
         for (int col = 0; col < COLS; col++) {
            if (board.getCell(row, col).getType().equals(CellType.FOOD)) {
               return;
            }
         }
      }

      fail("No FOOD cell found");


   }

   private void checkNull(Object object, String description) {
      assertNotNull(description + " object is null", object);
   }
}
