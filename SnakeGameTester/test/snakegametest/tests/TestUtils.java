/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.tests;

import com.snake.Board;
import com.snake.Cell;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

public class TestUtils {

	/**
	 *
	 * @param fieldName - Name of field
	 * @param expectedValue - Expected value of field
	 * @param object - Object to extract field from
	 */
	public static void testIntField(String fieldName, int expectedValue, Object object) {
		try {
			Field field = object.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			int fieldVal = field.getInt(object);

			assertEquals(fieldName + " value is incorrect (may not have been set),", expectedValue, fieldVal);

		} catch (NoSuchFieldException e) {
			fail("Missing required field: sideSize");
		} catch (Exception ex) {
			fail("Unknown exception occurred: " + ex.getMessage());
		}
	}

	public static void testObjectField(String fieldName, Object expectedValue, Object object, boolean testString) {
		try {

			assertNotNull("Object is null", object);

			Object fieldVal = getFieldValue(fieldName, object);

			assertNotNull(fieldName + " value is null", fieldVal);
			if (testString) {
				fieldVal = fieldVal.toString();
			}
			assertEquals(fieldName + " value is incorrect (may not have been set),", expectedValue, fieldVal);

		} catch (Exception ex) {
			fail("Unknown exception occurred: " + ex.getMessage());
		}
	}

    public static boolean checkFieldExists(String fieldName, Object object) {
       try {
          object.getClass().getDeclaredField(fieldName);
          return true;
       } catch (NoSuchFieldException ex) {
          fail(fieldName + " field does not exist!");
          return false;
       }
    }

	public static Object getFieldValue(String fieldName, Object object) {
		Field field;
		try {
			field = object.getClass().getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(object);
		} catch (Exception e) {
			return null;
		}
	}
	/**
	 * Check a method with a given name exists in the given class. If the class has arguments it
	 * should be passed in as an array of Class<?> objects.
	 * 
	 * If checking a method in the UI classes, prepend 'ui.' to the className
	 * @param className
	 * @param methodName
	 * @param parameterTypes
	 */
	public static void checkMethodExists(String className, String methodName, Class<?>... parameterTypes) {
		try {
			Class<?> subjectClass = Class.forName("com.snake." + className);
			subjectClass.getDeclaredMethod(methodName, parameterTypes);
		} catch (ClassNotFoundException ex) {
			fail(className + " class has not been implemented");
		} catch (NoSuchMethodException ex) {
			fail(methodName + "() has not been implemented");
		}
	}

      public static Cell findHead(Board board, int rows, int cols) {

      // Set head to null initially
      Cell head = null;

      for (int i = 0; i < rows; i++) {
         for (int j = 0; j < cols; j++) {
            Cell cell = board.getCell(i, j);
            // Make sure we only have 1 head cell (why we have null check)
            if (cell.getType().toString().equals("HEAD")) {
               if (head == null) {
                  head = cell;
               } else {
                  fail("Found more than one 'HEAD' cell");
               }
            }
         }
      }
      if (head == null) {
         fail("No cell marked as 'HEAD'");
      }
      return (Cell) head;
   }

}
