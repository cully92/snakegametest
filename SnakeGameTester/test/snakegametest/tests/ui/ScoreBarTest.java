/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.tests.ui;

import com.snake.ui.ScoreBar;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import snakegametest.runner.Task;
import snakegametest.tests.TestUtils;

/**
 * @author Sean
 */
public class ScoreBarTest {

   private ScoreBar bar = ScoreBar.getInstance();

   @Task(2.1)
   @Test
   public void testSetStatusMethod() throws IllegalAccessException, InvocationTargetException {
      String testString = "Test tings";

      // Need to do it this way instead of using DoesMethodExist because the method in the superclass
      // isn't called setStatus - in future could add this to template
      try {
         Method setStatus = ScoreBar.class.getMethod("setStatus", String.class);
         setStatus.invoke(bar, testString);
      } catch (NoSuchMethodException ex) {
         fail("setStatus method doesn't exist, or doesn't take a String parameter");
      }

      // Check status label has been changed
      JLabel statusLabel = (JLabel) TestUtils.getFieldValue("statusLabel", bar);
      assertTrue("statusLabel has not been changed", statusLabel.getText().equals(testString));
   }
}
