/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.tests.ui;

import static org.junit.Assert.*;

import org.junit.Test;

import snakegametest.runner.Task;

import com.snake.ui.SnakeUI;
import java.awt.event.KeyListener;

/**
 * @author Sean
 */
public class SnakeUITest {

   public SnakeUITest() {
   }

	@Task(1.1)
   @Test
   public void testTitleChanged() {
      SnakeUI snakeUI = new SnakeUI();
      String title = snakeUI.getTitle();
      assertTrue("Title hasn't been changed", !title.equals("Your title here"));
   }

    @Task(2.4)
    @Test
    public void testKeyListenerAdded() {
       SnakeUI ui = new SnakeUI();
      KeyListener[] keyListeners = ui.getKeyListeners();
      for (KeyListener listener : keyListeners) {
         String listenerClassName = listener.getClass().toString();
         if (listenerClassName.equals("class com.snake.SnakeKeyAdapter"))
            return;
      }

      fail("SnakeKeyAdapter class not attached to SnakeUI");
    }

}
