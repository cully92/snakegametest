/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.tests.ui;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;

import org.junit.Test;

import snakegametest.runner.Task;
import snakegametest.tests.TestUtils;

import com.snake.Board;
import com.snake.ui.BoardUI;

public class BoardUITest {

	private final int ROWS = 20;
	private final int COLS = 40;
	private final int SIDE_SIZE = 15;

	private final Board board = new Board(SIDE_SIZE, ROWS, COLS);
	private final BoardUI boardUI = new BoardUI(board, ROWS, COLS);

	@Task(1.44)
	@Test
	public void testBoardField() throws Exception {
		try {
			Field field = BoardUI.class.getDeclaredField("board");
			field.setAccessible(true);
			Object val = field.get(boardUI);

			assertNotNull("board field is null (may not have been set),", val);
			assertTrue(((Board)val).equals(board));
		} catch (NoSuchFieldException e) {
			fail("Missing required field: board");
		}
	}

	@Task(1.44)
	@Test
	public void testRowsField() {
		TestUtils.testIntField("rows", ROWS, boardUI);
	}

	@Task(1.44)
	@Test
	public void testColsField() {
		TestUtils.testIntField("cols", COLS, boardUI);
	}
}
