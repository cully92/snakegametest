package snakegametest.tests;

import com.snake.*;
import com.snake.templates.AbstractBoard;
import com.snake.templates.AbstractCell;
import com.snake.templates.AbstractSnake;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import snakegametest.runner.Task;
import static snakegametest.tests.TestUtils.*;

/**
 *
 * @author Sean
 */
public class SnakeTest {
   private final String GAMEOVER = "com.snake.GameOverException";

   private AbstractBoard board;
   private AbstractSnake snake;
   private final int ROWS = 20;
   private final int COLS = 40;
   private final int SIDE_SIZE = 15;

   public SnakeTest() {
   }

   @Before
   public void before() {
      board = new Board(SIDE_SIZE, ROWS, COLS);
      snake = board.getSnake();

   }

   @Task(1.71)
   @Test
   public void testBoardField() {
      testObjectField("board", board, snake, false);
   }

   @Task(1.74)
   @Test
   public void testPlaceMethodPlacesSnakeHead() {
      checkMethodExists("Snake", "place");
      snake.place();
      findHead((Board) board, ROWS, COLS);
   }

   @Task(1.8)
   @Test
   public void testPlaceMethodPlacesSnakeBody() {
      checkMethodExists("Snake", "place");
      snake.place();

      findCellWithinGivenSpacesOfHeadCell("BODY", 1);
   }

   @Task(1.8)
   @Test
   public void testPlaceMethodPlacesSnakeTail() {
      checkMethodExists("Snake", "place");
      snake.place();

      findCellWithinGivenSpacesOfHeadCell("TAIL", 2);
   }

   @Task(2.2)
   @Test
   public void testDirectionEnumExists() {
      Class dirClass = getClass("com.snake.Direction");

      try {
         Enum.valueOf(dirClass, "NORTH");
         Enum.valueOf(dirClass, "EAST");
         Enum.valueOf(dirClass, "SOUTH");
         Enum.valueOf(dirClass, "WEST");
      } catch (IllegalArgumentException ex) {
         fail("Direction enum doesn't contain NORTH, EAST, SOUTH and WEST");
      }
   }

   @Task(2.3)
   @Test
   public void testDirectionFieldExists() {
      checkFieldExists("dir", snake);
   }

   @Task(2.3)
   @Test
   public void testSetDirectionMethod() {
      Class<?> dirClass = getClass("com.snake.Direction");
      checkMethodExists("Snake", "setDirection", dirClass);

      Direction dir = Direction.EAST;
      snake.setDirection(dir);

      testObjectField("dir", dir.toString(), snake, true);
   }

   @Task(2.8)
   @Test
   public void testHeadField() {
      snake.place();

      checkFieldExists("head", snake);
   }

   @Task(2.8)
   @Test
   public void testBodyField() {
      snake.place();

      checkFieldExists("body", snake);
      List<Cell> body = (List<Cell>) getFieldValue("body", snake);
      if (body.isEmpty()) {
         fail("body field has no contents in the list");
      }
   }

   @Task(2.8)
   @Test
   public void testTailField() {
      snake.place();

      checkFieldExists("tail", snake);
   }

   @Task(2.9)
   @Test
   public void testMoveMethodMovesSnakeHead() throws Exception {
      // Get the Snake to the required state for the test
      snake.place();
      snake.setDirection(Direction.EAST);
      Cell head = findHead((Board) board, ROWS, COLS);;
      int newRow = head.getRow();
      int newCol = head.getCol() + 1;

      snake.move();
      head = findHead((Board) board, ROWS, COLS);
      if (!(head.getRow() == newRow && head.getCol() == newCol)) {
         fail("Move method does not move Snake correctly");
      }
   }

   @Task(3.2)
   @Test
   public void testUpdateDirectionsUpdatesDirectionOfHeadCell() throws Exception {

      Direction dir = Direction.WEST;

      // Setup
      snake.place();
      snake.setDirection(dir);

      snake.updateDirections();

      Cell head = findHead((Board) board, ROWS, COLS);
      assertTrue("Head direction wasn't changed by updateDirections()", head.getDirection().equals(dir));
   }

   @Task(3.2)
   @Test
   public void testUpdateDirectionsUpdatesDirectionOfBodyCell() throws Exception {

      // Setup
      snake.place();
      // Change the direction and update directions once
      // This is so that we can be sure the directions are changing
      // properly
      snake.setDirection(Direction.SOUTH);
      snake.updateDirections();
      Cell head = findHead((Board) board, ROWS, COLS);
      Direction dir = head.getDirection();

      snake.updateDirections();
      Cell body = findCellWithinGivenSpacesOfHeadCell("BODY", 1);

      assertTrue("Body direction wasn't changed by updateDirections()", body.getDirection().equals(dir));
   }

   @Task(3.2)
   @Test
   public void testUpdateDirectionsUpdatesDirectionOfTailCell() throws Exception {

      // Setup
      snake.place();
      // Change the direction and update directions once
      // This is so that we can be sure the directions are changing
      // properly
      snake.setDirection(Direction.SOUTH);
      snake.updateDirections();

      Cell body = findCellWithinGivenSpacesOfHeadCell("BODY", 1);
      Direction dir = body.getDirection();

      snake.updateDirections();
      Cell tail = findCellWithinGivenSpacesOfHeadCell("TAIL", 2);

      assertTrue("Tail direction wasn't changed by updateDirections()", tail.getDirection().equals(dir));
   }

   @Task(3.3)
   @Test
   public void testMoveCellMethodExists() {
      checkMethodExists("Snake", "moveCell", Cell.class, CellType.class);
   }

   @Task(3.4)
   @Test
   public void testUpdatedMoveMethodMovesSnakeHead() throws Exception {
      snake.place();
      snake.setDirection(Direction.SOUTH);

      Cell headCell = findHead((Board) board, ROWS, COLS);
      int row = headCell.getRow();
      int col = headCell.getCol();
      Direction dir = headCell.getDirection();

      switch (dir) {
         case NORTH:
            row--;
            break;
         case EAST:
            col++;
            break;
         case WEST:
            col--;
            break;
         case SOUTH:
            row++;
            break;
      }

      snake.move();

      Cell newHeadCell = findHead((Board) board, ROWS, COLS);
      assertTrue("Move method moves the Snake's head", newHeadCell.getRow() == row);
      assertTrue("Move method moves the Snake's head", newHeadCell.getCol() == col);

   }

   @Task(3.4)
   @Test
   public void testUpdatedMoveMethodMovesSnakeBody() throws Exception {
      snake.place();
      snake.setDirection(Direction.SOUTH);

      Cell bodyCell = findCellWithinGivenSpacesOfHeadCell("BODY", 1);
      int row = bodyCell.getRow();
      int col = bodyCell.getCol();
      Direction dir = bodyCell.getDirection();

      switch (dir) {
         case NORTH:
            row--;
            break;
         case EAST:
            col++;
            break;
         case WEST:
            col--;
            break;
         case SOUTH:
            row++;
            break;
      }

      snake.move();

      Cell newBodyCell = findCellWithinGivenSpacesOfHeadCell("BODY", 1);
      assertTrue("Move method moves the Snake", newBodyCell.getRow() == row);
      assertTrue("Move method moves the Snake", newBodyCell.getCol() == col);

   }

   @Task(3.4)
   @Test
   public void testUpdatedMoveMethodMovesSnakeTail() throws Exception {
      snake.place();
      snake.setDirection(Direction.SOUTH);

      Cell bodyCell = findCellWithinGivenSpacesOfHeadCell("TAIL", 2);
      int row = bodyCell.getRow();
      int col = bodyCell.getCol();
      Direction dir = bodyCell.getDirection();

      switch (dir) {
         case NORTH:
            row--;
            break;
         case EAST:
            col++;
            break;
         case WEST:
            col--;
            break;
         case SOUTH:
            row++;
            break;
      }

      snake.move();

      Cell newBodyCell = findCellWithinGivenSpacesOfHeadCell("TAIL", 2);
      assertTrue("Move method moves the Snake", newBodyCell.getRow() == row);
      assertTrue("Move method moves the Snake", newBodyCell.getCol() == col);

   }

   @Task(3.6)
   @Test
   public void testCheckCollisionDoesntThrowExceptionForFoodCell() throws Exception {
      checkMethodExists("Snake", "checkCollision", int.class, int.class);
      Board board = (Board) this.board;
      // Start board first to start timer
      board.start();

      board.getCell(4, 5).setType(CellType.FOOD);
      try {
         ((Snake) snake).checkCollision(4, 5);
      } catch (Exception ex) {
         if (!ex.getClass().getName().equals(GAMEOVER)) {
            fail("GameOverException thrown when Snake meets food cell");
         } else {
            throw ex;
         }
      }
   }

   @Task(3.6)
   @Test
   public void testCheckCollisionThrowsExceptionForSnakeBodyCell() throws Exception {
      checkMethodExists("Snake", "checkCollision", int.class, int.class);
      Board board = (Board) this.board;
      // Start board first to start timer
      board.start();

      board.getCell(4, 5).setType(CellType.BODY);
      try {
         ((Snake) snake).checkCollision(4, 5);
      } catch (Exception ex) {
         if (!ex.getClass().getName().equals(GAMEOVER)) {
            fail("GameOverException wasn't thrown when Snake meets tail cell. Original exception: " + ex.getMessage());
         } else {
            // Do nothing, we expect a GameOverException
         }
      }
   }

   @Task(3.6)
   @Test
   public void testCheckCollisionThrowsExceptionForSnakeTailCell() throws Exception {
      checkMethodExists("Snake", "checkCollision", int.class, int.class);
      Board board = (Board) this.board;
      board.start();

      board.getCell(4, 5).setType(CellType.TAIL);
      try {
         ((Snake) snake).checkCollision(4, 5);
      } catch (Exception ex) {
         if (!ex.getClass().getName().equals(GAMEOVER)) {
            fail("GameOverException wasn't thrown when Snake meets tail cell. Original exception: " + ex.getMessage());
         } else {
            // Do nothing - we expect a GameOverException
         }
      }
   }

   @Task(3.6)
   @Test
   public void testScoreField() {
      checkFieldExists("score", snake);
   }

   /**
    * Finds the head cell, then checks 'spaces' number of spaces either side of
    * the cell, to try and find a cell of the given type
    */
   private Cell findCellWithinGivenSpacesOfHeadCell(String type, int spaces) {

      AbstractCell head = findHead((Board) board, ROWS, COLS);
      int headRow = head.getRow();
      int headCol = head.getCol();

      for (int i = headRow - spaces; i <= headRow + spaces && i < ROWS; i += spaces) {
         for (int j = headCol - spaces; j <= headCol + spaces && j < COLS; j += spaces) {
            if (i < 0 || j < 0) {
               continue;
            }

            AbstractCell cell = board.getCell(i, j);
            if (cell.getType().toString().equals(type)) {
               return (Cell) cell;
            }
         }
      }

      fail("No " + type + " cell found within " + spaces + " space of HEAD cell");
      return null;
   }

   @Task(3.7)
   @Test
   public void testSnakeGrowsAfterEatingFood() throws Exception {

      // The snake grows if it detects a collision with food
      snake.place();
      board.getCell(4, 5).setType(CellType.FOOD);

      List<Cell> body = (List<Cell>) getFieldValue("body", snake);
      int origBodySize = body.size();

      ((Snake) snake).checkCollision(4, 5);

      body = (List<Cell>) getFieldValue("body", snake);
      int newBodySize = body.size();

      assertTrue("Snake didn't grow after eating food", newBodySize == (origBodySize + 1));
   }

   /**
    * Check a class with the given (fully qualified) name exists.
    *
    * @param className
    */
   private Class<?> getClass(String className) {
      try {
         return Class.forName(className);
      } catch (ClassNotFoundException ex) {
         fail(className + " class doesn't exist");
         return null;
      }
   }
}
