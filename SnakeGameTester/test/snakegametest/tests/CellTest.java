package snakegametest.tests;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static snakegametest.tests.TestUtils.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import snakegametest.runner.Task;

import com.snake.Cell;
import com.snake.CellType;
import com.snake.Direction;

public class CellTest {
	
	private final int COL = 3;
	private int ROW = 2;
	private int SIDE_SIZE = 15;
	private Cell cell = new Cell(SIDE_SIZE, ROW, COL);

	private final String CLASS_NAME = "Cell";

   @Task(1.42)
	@Test
	public void testSideSizeField() {
		testIntField("sideSize", SIDE_SIZE, cell);
	}

	@Task(1.42)
	@Test
	public void testRowField() {
		testIntField("row", ROW, cell);
	}

	@Task(1.42)
	@Test
	public void testColField() {
		testIntField("col", COL, cell);
	}

	@Task(1.5)
	@Test
	public void testCellType() {
		CellType[] values = CellType.values();

		// Map of the expected values to store if they are all found in the CellType
		Map<String, Boolean> expected = new HashMap<String, Boolean>();
		expected.put("EMPTY", false);
		expected.put("HEAD", false);
		expected.put("BODY", false);
		expected.put("TAIL", false);

		// Update the hash map to true for each type it finds
		for (CellType value : values) {
			expected.put(value.toString(), true);
		}

		// Check through the hash map, and make sure everything is true, if it isn't
		// then add it to the 'missing' string to display at the end.
		String missing = "";
		for (String key : expected.keySet()) {
			if (!expected.get(key).booleanValue()) {
				missing += key + ", ";
			}
		}

		if (!missing.equals("")) {
			fail("CellType enum missing following values: " + missing);
		}

	}

	@Task(1.5)
	@Test
	public void testTypeField() {
		testObjectField("type", "EMPTY", cell, true);
	}

	@Task(1.5)
	@Test
	public void testGetTypeReturnsType() {
		checkMethodExists(CLASS_NAME, "getType");

		CellType type = cell.getType();

		assertNotNull("getType() returns null", type);
	}

	@Task(1.5)
	@Test
	public void testSetTypeMethod() {
		
		checkMethodExists(CLASS_NAME, "setType", CellType.class);
		cell.setType(null);

		CellType type = (CellType) getFieldValue("type", cell);
		assertNull("setType() works", type);

	}

    @Task(2.7)
    @Test
    public void testDirectionField() {
       checkFieldExists("dir", cell);
       Direction dir = (Direction) getFieldValue("dir", cell);
       
       assertNotNull("dir field is null", dir);
       if (!dir.equals(Direction.NONE))
          fail("Default direction is not 'Direction.NONE'");
    }

    @Task(2.7)
    @Test
    public void testSetDirectionMethod() {
       checkMethodExists(CLASS_NAME, "setDirection", Direction.class);

       cell.setDirection(Direction.NORTH);
       Direction result = (Direction) getFieldValue("dir", cell);
       assertTrue("setDirection method doesn't set the direction", result.equals(Direction.NORTH));

    }

    @Task(2.7)
    @Test
    public void testGetDirectionMethod() {
       checkMethodExists(CLASS_NAME, "getDirection");

       Direction result = cell.getDirection();
       if (!result.equals(Direction.NONE))
          fail("Get direction method doesn't return Direction.NONE by default");
    }

    @Task(2.7)
    @Test
    public void testGetRowMethod() {
       checkMethodExists(CLASS_NAME, "getRow");

       int row = cell.getRow();
       if (row != this.ROW)
          fail("getRow didn't return the correct value");
    }

    @Task(2.7)
    @Test
    public void testGetColMethod() {
       checkMethodExists(CLASS_NAME, "getCol");

       int col = cell.getCol();
       if (col != this.COL)
          fail("getCol didn't return the correct value");
    }
}
