package snakegametest.runner;

import org.junit.runner.RunWith;

/**
 * Run this file as a JUnit Test to run the tests through the custom JUnit 4 runner
 * infrastructure.
 * 
 * @author Sean
 */
@RunWith(SnakeSuite.class)
public class SnakeGameTestRunner {
	
}
