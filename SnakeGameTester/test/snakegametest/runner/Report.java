package snakegametest.runner;

import java.io.*;
import java.lang.reflect.Method;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.*;

/**
 * Class representing a report, holding all the current SnakeTestResults, and
 * updated after a test run has finished. Default constructor reads report from
 * report.txt, alternate constructor reads from the file name passed in
 *
 */
public class Report {

   private Map<Double, List<SnakeTestResult>> records = new HashMap<Double, List<SnakeTestResult>>();
   private int port = 7;
   private String hostName = "192.168.0.11";

   public Report() {
      readFromFile("report.txt");
   }

   public Report(String fileName) {
      readFromFile(fileName);
   }

   /**
    * Read a saved report into the current report object.
    */
   private void readFromFile(String fileName) {

      List<String> lines = getLinesFromFile(fileName);

      populateFromList(lines);
   }

   /**
    * Populate the report from lines read from a file
    */
   private void populateFromList(List<String> lines) {
      for (String line : lines) {
         // Split each value into a different String
         String[] values = line.split("\t");

         if (line.startsWith("Exercise")) {
            createResultFromValues(values);
         }
      }
   }

   /**
    * Return the lines of the given file as Strings
    * @param fileName
    * @return 
    */
   public static List<String> getLinesFromFile(String fileName) {
      File file = new File(fileName);
      List<String> lines = new ArrayList<String>();
      if (!file.exists()) {
         return lines;
      }

      BufferedReader reader = null;
      try {
         reader = new BufferedReader(new FileReader(file));

         String line;
         while ((line = reader.readLine()) != null) {
            lines.add(line);
         }

      } catch (Exception e) {
         e.printStackTrace();
      } finally {
         try {
            if (reader != null) {
               reader.close();
            }
         } catch (IOException e) {
         }
      }
      return lines;
   }

   /**
    * Write the report to the default file (report.txt)
    */
   public void writeToFile() {
      writeToFile("report.txt");
   }

   /**
    * Method to write the report to file, for use by the next run
    */
   public void writeToFile(String file) {

      // Don't write it if somethings gone wrong!
      if (records.isEmpty()) {
         return;
      }

      BufferedWriter writer = null;
      try {
         File reportFile = new File(file);
         if (!reportFile.exists()) {
            reportFile.createNewFile();
         }

         writer = new BufferedWriter(new FileWriter(reportFile));
         for (Double task : getSortedQuestions()) {
            for (SnakeTestResult results : records.get(task)) {
               writer.write(results.toString());
            }
         }

      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try {
            writer.close();
         } catch (IOException e) {
         }
      }
   }

   /**
    * Send the report to the server, where it will be collated.
    */
   public void sendToServer() {
      ReportClient client = new ReportClient();
      client.sendToServer();
   }

   /**
    * Add a given test result to the report object
    *
    * @param taskNumber
    * @param method
    */
   public void addTestResult(double taskNumber, Method method) {
      if (!records.containsKey(taskNumber)) {
         records.put(taskNumber, new ArrayList<SnakeTestResult>());
      }

      records.get(taskNumber).add(new SnakeTestResult(taskNumber, method));
   }

   /**
    * Get the SnakeTestResults for a given task number. There may be more than
    * one, if a task has multiple tests associated with it.
    *
    * @param taskNumber
    * @return
    */
   public List<SnakeTestResult> getResults(double taskNumber) {
      return records.get(taskNumber);
   }

   /**
    * Create the correct SnakeTestResult from values in the file
    */
   private void createResultFromValues(String[] values) {
      String task = stripValue(values[0]);
      String run = stripValue(values[1]);
      String failed = stripValue(values[3]);
      String regressed = stripValue(values[4]);
      String methodName = values[6];
      double taskNo = Double.parseDouble(task);

      String hintsAndSol = values[5];
      boolean hint = false, sol = false;
      if (!hintsAndSol.isEmpty()) {
         hint = hintsAndSol.substring(1, 2).equals("1");
         sol = hintsAndSol.substring(3).equals("1");
      }

      List<SnakeTestResult> results = records.get(taskNo);
      if (results == null) {
         // Initialise it if it doesn't already exist
         results = new ArrayList<SnakeTestResult>();
         SnakeTestResult result = new SnakeTestResult(taskNo, getMethod(taskNo, methodName));

         result.updateFromFile(run, failed, regressed, hint, sol);
         results.add(result);

         records.put(taskNo, results);
      } else {
         boolean resultFoundInReport = false;
         for (SnakeTestResult result : results) {
            // May be multiple tests associated with this task, so look up using the
            // method name as well.
            if (result.getMethod().getName().equals(methodName)) {
               result.updateFromFile(run, failed, regressed, hint, sol);
               resultFoundInReport = true;
               break;
            }
         }

         if (!resultFoundInReport) {
            addTestResult(taskNo, getMethod(taskNo, methodName));
            createResultFromValues(values);
         }
      }
   }

   /**
    * Get the given test method for a particular task.
    *
    * @param taskNo
    * @param methodName
    * @return
    */
   public Method getMethod(double taskNo, String methodName) {
      Class<?>[] testClasses = ProgressTracker.getTestClasses();

      // Loop through all test classes to find which one contains the
      // Method
      for (Class<?> clazz : testClasses) {
         Method[] methods = clazz.getMethods();
         for (Method method : methods) {
            Task taskAnnotation = method.getAnnotation(Task.class);
            if (taskAnnotation == null) {
               continue;
            }

            boolean correctTask = taskAnnotation.value() == taskNo;
            if (correctTask && method.getName().equals(methodName)) {
               return method;
            }
         }
      }

      throw new RuntimeException("No method called " + methodName + " found");
   }

   /**
    * Strip the actual value out of a String, e.g "Exercise 1"
    */
   private static String stripValue(String initialText) {
      return initialText.split(" ")[1];
   }

   /**
    * Get the sorted questions, used because the keySet is returned unordered.
    */
   private List<Double> getSortedQuestions() {
      Set<Double> qs = records.keySet();
      Double[] questions = qs.toArray(new Double[0]);
      Arrays.sort(questions);
      return Arrays.asList(questions);
   }

   /**
    * Set that the hint for a given task has been accessed, and write to file
    *
    * @param taskNum
    */
   public void setHintAccessed(double taskNum) {
      List<SnakeTestResult> results = records.get(taskNum);

      if (results == null) {
         return;
      }

      for (SnakeTestResult result : results) {
         result.setHintAccessed();
      }

      writeToFile();
   }

   /**
    * Set that the solution for a given task has been accessed, and write to
    * file
    *
    * @param taskNum
    */
   public void setSolutionAccessed(double taskNum) {
      List<SnakeTestResult> results = records.get(taskNum);

      if (results == null) {
         return;
      }

      for (SnakeTestResult result : results) {
         result.setSolutionAccessed();
      }

      writeToFile();
   }

   /**
    * Check if the solution is available. A solution becomes available when the
    * task has been failed three times
    */
   public boolean isSolutionAvailable(double taskNumber) {
      List<SnakeTestResult> results = records.get(taskNumber);
      boolean solutionAvailable = false;
      if (results == null) {
         return false;
      }

      for (SnakeTestResult result : results) {
         if (result.getFailCount() >= 3) {
            solutionAvailable = true;
         }
      }

      return solutionAvailable;
   }

   /**
    * Calculate the score for the whole report. Scores are calculated as
    * follows: Initial: +2 Hint accessed: -0.5 Solution accessed: -1
    *
    * @return
    */
   public double calculateScore() {
      double currentScore = 0;

      for (List<SnakeTestResult> results : records.values()) {
         for (SnakeTestResult result : results) {
            currentScore += result.getScore();
         }
      }

      return currentScore;
   }

   /**
    * Check if any results exist for the given task and method
    *
    * @param task
    * @param method
    * @return
    */
   public boolean resultExists(double task, Method method) {
      boolean resultExists = false;
      List<SnakeTestResult> results = records.get(task);
      if (results == null) {
         return false;
      }

      for (SnakeTestResult result : results) {
         if (result.getMethod().equals(method)) {
            resultExists = true;
         }
      }

      return resultExists;
   }

   /**
    * Internal class to deal with sending the report to server
    */
   class ReportClient {

      /**
       * Send the report to the server
       */
      public void sendToServer() {
         String ERROR_MSG = "Error connecting to the server. Please check the SnakeServer is running.";

         try {

            //Connect to the server
            getConnectionDetails();
            Socket socket = new Socket(hostName, port);
            System.out.println("Connected to the server!");
            System.out.println("------------------------");
            System.out.println("Socket address: " + socket.getLocalSocketAddress());
            System.out.println("IP: " + socket.getInetAddress());
            System.out.println("Port: " + socket.getPort());
            System.out.println("------------------------");

            // Create writer/reader objects to communicate with server
            PrintWriter serverOutput = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader serverInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            // Get the lines from the report file and
            List<String> reportLines = getLinesFromFile("report.txt");
            sendReport(serverInput, serverOutput, reportLines);

         } catch (UnknownHostException ex) {
            System.out.println(ERROR_MSG);
            System.out.println("UnknownHostException: " + ex.getMessage());
         } catch (IOException ex) {
            System.out.println(ERROR_MSG);
            ex.printStackTrace();
         }
      }

      /**
       * Helper method to send the report to the servers input, and receive
       * output from the servers output. Simple approach, by sending
       *
       * @param input
       * @param output
       * @param reportLines
       * @throws IOException
       */
      private void sendReport(BufferedReader input, PrintWriter output, List<String> reportLines) throws IOException {
         String fromServer;
         int count = 0;
         System.out.println("Sending report...");
         while ((fromServer = input.readLine()) != null) {
            if (!fromServer.equals("Input received")) {
               System.out.println("Server: " + fromServer);
            }

            // Exit condition
            if (fromServer.equals("Bye.")) {
               break;

               // Once connection started, send the username
            } else if (fromServer.equals("Connection initiated")) {
               output.println("UN:" + System.getProperty("user.name"));


               // Tell server the report is finished
            } else if (count >= reportLines.size()) {
               output.println("REPORTEND");

               // Send the report line
            } else {
               // send the report line
               output.println(reportLines.get(count));
               count++;
            }
         }
      }

      /**
       * Read the connection details from a file called "conn.txt", and set the
       * port and hostname details
       */
      private void getConnectionDetails() {
         List<String> connLines = getLinesFromFile("conn.txt");
         if (connLines.size() >= 2) {
            hostName = connLines.get(0);
            port = Integer.parseInt(connLines.get(1));
         }
         System.out.println("Connection details being used:");
         System.out.println("Host name: " + hostName + " Port: " + port);
         System.out.println();
      }
   }
}