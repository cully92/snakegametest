package snakegametest.runner;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;

/**
 * Custom JUnit4 runner to allow adaptive testing. By tracking the progress of each
 * run, only run the tests that correspond to tasks up to the current task.
 */
public class SnakeClassRunner extends BlockJUnit4ClassRunner {

	private double nextTask;

	public SnakeClassRunner(Class<?> klass) throws InitializationError {
		super(klass);
	}

	/** 
	 * Validate the instance methods but don't add any errors for classes without
	 * runnable methods as this will happen at the start before the user has progressed.
	 * 
	 * @see org.junit.runners.BlockJUnit4ClassRunner#validateInstanceMethods(java.util.List)
	 */
	@Override
	protected void validateInstanceMethods(List<Throwable> errors) {
		validatePublicVoidNoArgMethods(After.class, false, errors);
		validatePublicVoidNoArgMethods(Before.class, false, errors);
		validateTestMethods(errors);
	}


	/**
	 * Compute which test methods to run, by getting the value of the next task from
	 * the ProgressTracker class
	 */
	@Override
	protected List<FrameworkMethod> computeTestMethods() {
		nextTask = ProgressTracker.getNextTask();

		List<FrameworkMethod> methodsToRun = new ArrayList<FrameworkMethod>();
		List<FrameworkMethod> allMethods = getTestClass().getAnnotatedMethods(Test.class);

		for (FrameworkMethod method : allMethods) {
			Task taskAnnotation = method.getAnnotation(Task.class);
			if (taskAnnotation != null) {
				if (taskAnnotation.value() <= nextTask) {
					methodsToRun.add(method);
				}
			}
		}

		return methodsToRun;
	}
}
