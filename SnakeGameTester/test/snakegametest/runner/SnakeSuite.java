package snakegametest.runner;

import java.util.ArrayList;
import java.util.List;
import org.junit.runner.Runner;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.Suite;
import org.junit.runners.model.InitializationError;
import snakegametest.runner.ProgressTracker.ResultCollector;

/**
 * Run the Snake tests as a Suite, adding a SnakeRunListener and logging results.
 */
public class SnakeSuite extends Suite {

	public SnakeSuite(Class<?> klass) throws InitializationError {

		super(klass, getRunners(klass));
	}

	/**
	 *	Create and return a new CustomSnakeRunner for each test class - no longer uses the @SuiteClass annotation
     *  as that would require the user to update the SnakeGameTestRunner class each time a new test class is
     *  added
	 */
	private static List<Runner> getRunners(Class<?> klass) throws InitializationError {
		List<Runner> runners = new ArrayList<Runner>();

        // Add all classes with tests - removes reliance on the user updating the SnakeSuite
        Class<?>[] classes = ProgressTracker.getTestClasses();

		for (Class<?> annotatedClass : classes) {
			runners.add(new SnakeClassRunner(annotatedClass));
		}
		
		return runners;
	}
	
	@Override
	public void run(RunNotifier notifier) {
		// Add a SnakeRunListener before running the tests
		notifier.addListener(new SnakeRunListener());
        ResultCollector.reset();
		super.run(notifier);
        logTestRun();
	}

    /**
     * Log that the test run has finished
     */
    private void logTestRun() {
      List<Failure> failures = ResultCollector.getFailures();

      if (failures.isEmpty()) {
			double lastTask = ProgressTracker.getNextTask();
			ProgressTracker.setLastTaskCompleted(lastTask);
		}

      ProgressTracker.logResults();
    }

}
