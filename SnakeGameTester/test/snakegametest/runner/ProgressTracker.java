package snakegametest.runner;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.runner.notification.Failure;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.TestClass;
import snakegametest.hint.HintUI;
import snakegametest.tests.BoardTest;
import snakegametest.tests.CellTest;
import snakegametest.tests.SnakeTest;
import snakegametest.tests.ui.BoardUITest;
import snakegametest.tests.ui.ScoreBarTest;
import snakegametest.tests.ui.SnakeUITest;

/**
 * Class to track the progress of the user, uses reading from / writing to files
 * to achieve this.
 *
 * @author Sean
 */
public class ProgressTracker {

   private static Report report;
   private static List<Double> tasks = null;
   private static double lastTaskCompleted = 1.0;
   private static volatile boolean hintUIRunning = false;

   /**
    * Get the next task number to display, based on the users progress
    */
   public static double getNextTask() {

      if (tasks == null) {
         getTaskNumbers();
         getLastTaskFromFile();
      }

      double lastTask = lastTaskCompleted;
      for (int i = 0; i < tasks.size(); i++) {
         double currentTask = tasks.get(i);
         if (currentTask == lastTask) {
            if ((i + 1) <= tasks.size() - 1) {
               return tasks.get(i + 1);
            }
         } else if (currentTask > lastTask) {
            return currentTask;
         }
      }
      return tasks.get(tasks.size() - 1);
   }

   /**
    * Set the last task that has been successfully completed
    *
    * @param taskNumber
    */
   public static void setLastTaskCompleted(double taskNumber) {
      lastTaskCompleted = taskNumber;

      BufferedWriter writer = null;
      try {
         writer = new BufferedWriter(new FileWriter("progress.txt"));
         writer.write("" + taskNumber);
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try { writer.close(); }
         catch (IOException e) {
         }
      }
   }

   /**
    * Get the task numbers, this only needs to be executed once at the start. It
    * does this by querying the task annotations and getting the values
    */
   private static void getTaskNumbers() {

      tasks = new ArrayList<Double>();
      report = new Report();

      Class<?>[] classes = getTestClasses();

      for (Class<?> aClass : classes) {
         TestClass classs = new TestClass(aClass);
         List<FrameworkMethod> taskMethods = classs.getAnnotatedMethods(Task.class);
         for (FrameworkMethod method : taskMethods) {
            double taskNumber = method.getAnnotation(Task.class).value();
            // Only add task numbers we haven't already added
            if (!tasks.contains(taskNumber)) {
               tasks.add(taskNumber);
            }

            // Add a SnakeTestResult to the report for each task
            if (!report.resultExists(taskNumber, method.getMethod())) {
               report.addTestResult(taskNumber, method.getMethod());
            }
         }
      }

      Collections.sort(tasks);

   }

   /**
    * Read the last task number completed from the progress.txt file
    */
   private static void getLastTaskFromFile() {
      BufferedReader reader = null;
      try {
         reader = new BufferedReader(new FileReader("progress.txt"));
         String line = reader.readLine();
         lastTaskCompleted = Double.parseDouble(line);
      } catch (FileNotFoundException e) {
         // Valid, if tester not run before
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try {
            if (reader != null) {
               reader.close();
            }
         } catch (IOException e) {
         }
      }
   }

   /**
    * Log the results of a finished test run, held in the records data structure.
    * Also writes the report to file, and sends it to the server.
    */
   public static void logResults() {

      List<Double> failedTasks = new ArrayList<Double>();
      List<Failure> failures = ResultCollector.getFailures();
      for (Failure failure : failures) {
         // Get the task number
         failedTasks.add(failure.getDescription().getAnnotation(Task.class).value());
      }

      for (Double task : getUniqueValues(ResultCollector.getTasks())) {

         List<SnakeTestResult> testResult = report.getResults(task);
         // If there aren't any test results, then there's nothing to log as
         // a success or failure
         if (testResult == null) {
            continue;
         }

         if (!failedTasks.contains(task)) {
            for (SnakeTestResult snakeTestResult : testResult) {
               snakeTestResult.logTestSuccess();
            }
         } else {
            for (SnakeTestResult failedResult : testResult) {
               failedResult.logTestFailure();
            }
         }

      }

      report.writeToFile();
      report.sendToServer();

      // Only launch the hint window if there are no failures
      if (!failures.isEmpty()) {
         launchHintUI();
      }
   }

   /**
    * Launch the Hint UI, and wait until it closes before continuing.
    */
   private static void launchHintUI() {
      HintUI ui = new HintUI();
      ui.setVisible(true);

      ui.addWindowListener(new WindowAdapter() {

         @Override
         public void windowClosing(WindowEvent e) {
            super.windowClosing(e);
            hintUIRunning = false;
         }
      });

      hintUIRunning = true;
      while (hintUIRunning) {
         // Do nothing. i.e wait for HintUI to close
      }
   }

   /**
    * Get the (sorted) unique values from the original list
    */
   private static List<Double> getUniqueValues(List<Double> orig) {
      List<Double> uniqueValues = new ArrayList<Double>();

      for (double value : orig) {
         if (!uniqueValues.contains(value)) {
            uniqueValues.add(value);
         }
      }

      Collections.sort(uniqueValues);
      return uniqueValues;
   }

   /**
    * Get a list of all the test classes, doing it this way removes reliance on
    * the SuiteClasses annotation
    *
    * @return
    */
   public static Class<?>[] getTestClasses() {
      return new Class<?>[]{
                 SnakeUITest.class, BoardTest.class, CellTest.class,
                 BoardUITest.class, SnakeTest.class, ScoreBarTest.class
              };
   }

   /**
    * Class to collect results as the test run executes. This collects the task
    * numbers for tests executed, and the failure objects produced from tests
    * failing.
    */
   public static class ResultCollector {

      private static List<Double> executedTasks = new ArrayList<Double>();
      private static List<Failure> failures = new ArrayList<Failure>();

      /**
       * Add the value of an executed task
       */
      public static void addTask(double task) {
         executedTasks.add(task);
      }

      /**
       * Add a failed task
       */
      public static void addFailure(Failure failure) {
         failures.add(failure);
      }

      /**
       * Reset, i.e between runs
       */
      public static void reset() {
         failures.clear();
         executedTasks.clear();
      }

      public static List<Failure> getFailures() {
         return failures;
      }

      public static List<Double> getTasks() {
         return executedTasks;
      }
   }
}