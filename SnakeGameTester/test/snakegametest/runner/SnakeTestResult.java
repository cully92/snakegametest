package snakegametest.runner;

import java.lang.reflect.Method;

/**
 * Represents the cumulative result of a particular test, including whether or not the
 * hints and solutions have been accessed.
 */
public class SnakeTestResult {

	private double task;
	private Method method;
	private int failedCount = 0;
	private int runCount = 0;
	private int regressionCount = 0;
    
    // Whether or not the user has got the hint and/or sol
    private boolean hintGot = false;
    private boolean solGot = false;

	public SnakeTestResult (double task, Method method) {
		this.task = task;
		this.method = method;
	}
	
	/**
	 * Log that the given test has completed successfully
	 */
	public void logTestSuccess() {
		this.runCount++;
	}
	
	/**
	 * Log that the test has failed, if the test has previously passed
	 * this is logged as a regression.
	 */
	public void logTestFailure() {
		this.runCount++;
		this.failedCount++;
		
		if (this.runCount != this.failedCount)
			this.regressionCount++;
	}

	public Method getMethod() {
		return method;
	}

	public String toString() {
		String succeeded = "\tSucceeded: " + (runCount - failedCount);
        String hint = "H" + (hintGot ? "1" : "0");
        String sol = "S" + (solGot ? "1" : "0");
		return "Exercise " + task + "\tRun: " + runCount + succeeded +
					"\tFailed: " + failedCount + "\tRegressed: " + regressionCount + 
					"\t" + hint + sol + "\t" + method.getName() + "\n";
	}

	/**
	 * Access method to quickly update the report to the correct state with values from the file
	 * @param run
	 * @param failed
	 * @param regressed
	 */
	public void updateFromFile(String run, String failed, String regressed, boolean hintAccessed, boolean solAccessed) {
		this.runCount = Integer.parseInt(run);
		this.failedCount = Integer.parseInt(failed);
		this.regressionCount = Integer.parseInt(regressed);
        this.hintGot = hintAccessed;
        this.solGot = solAccessed;
	}

    public void setHintAccessed() {
       hintGot = true;
    }

    public void setSolutionAccessed() {
       solGot = true;
    }

    public int getFailCount() {
       return failedCount;
    }

    /**
     * Calculate the score for this result
     * Initial: 2
     * Hint accessed: -0.5
     * Solution accessed: -1
     * @return
     */
    public double getScore() {
       if (runCount == 0)
          return 0;

       double score = 2;
       if (hintGot)
          score -= 0.5;
       if (solGot)
          score -= 1;

       return score;
    }
	
}
