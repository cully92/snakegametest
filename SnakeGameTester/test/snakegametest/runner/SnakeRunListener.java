package snakegametest.runner;

import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import org.junit.runner.notification.RunListener;
import snakegametest.runner.ProgressTracker.ResultCollector;

/**
 * Custom RunListener to add the results from the unit tests to the ResultCollector
 */
public class SnakeRunListener extends RunListener {
	
   @Override
   public void testFailure(Failure failure) throws Exception {
      super.testFailure(failure);
      ResultCollector.addFailure(failure);
   }

   @Override
   public void testStarted(Description description) throws Exception {
      super.testStarted(description);
      // Add the task currently being tested
      ResultCollector.addTask(description.getAnnotation(Task.class).value());
   }
}