/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.runner.tests;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author Sean
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({snakegametest.runner.tests.ProgressTrackerTest.class, snakegametest.runner.tests.SnakeTestResultTest.class, snakegametest.runner.tests.RunnerTest.class, snakegametest.runner.tests.ReportTest.class,
                     snakegametest.socket.tests.SnakeResultProtocolTest.class})
public class SnakeGameTesterSuite {

}
