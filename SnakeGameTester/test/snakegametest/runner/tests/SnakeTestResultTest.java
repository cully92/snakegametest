package snakegametest.runner.tests;

import java.lang.reflect.Method;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import snakegametest.runner.Report;
import snakegametest.runner.SnakeTestResult;
import snakegametest.tests.TestUtils;

/**
 *
 * @author Sean
 */
public class SnakeTestResultTest {

   private SnakeTestResult result;
   private Method method;

   @Before
   public void before() {
      double task = 1.1;
      method = new Report().getMethod(task, "testTitleChanged");
      result = new SnakeTestResult(task, method);
   }

   @Test
   public void testLogSuccess() {
      result.logTestSuccess();

      Integer runCount = (Integer) TestUtils.getFieldValue("runCount", result);
      assertTrue(((int) runCount) == 1);
   }

   @Test
   public void testLogFailure() {
      result.logTestFailure();

      Integer runCount = (Integer) TestUtils.getFieldValue("runCount", result);
      Integer failCount = (Integer) TestUtils.getFieldValue("failedCount", result);
      Integer regressionCount = (Integer) TestUtils.getFieldValue("regressionCount", result);

      assertTrue(((int) runCount) == 1);
      assertTrue(((int) failCount) == 1);
      assertTrue(((int) regressionCount) == 0);
   }

   @Test
   public void testLogFailureRegression() {
      result.logTestSuccess();
      result.logTestFailure();

      Integer runCount = (Integer) TestUtils.getFieldValue("runCount", result);
      Integer failCount = (Integer) TestUtils.getFieldValue("failedCount", result);
      Integer regressionCount = (Integer) TestUtils.getFieldValue("regressionCount", result);

      assertTrue(((int) runCount) == 2);
      assertTrue(((int) failCount) == 1);
      assertTrue(((int) regressionCount) == 1);

   }

   @Test
   public void testGetMethod() {
      Method method = result.getMethod();

      assertTrue(method.equals(this.method));
   }

   @Test
   public void testUpdateFromFileMethod() {
      String runCount = "4";
      String failedCount = "3";
      String regressionCount = "2";
      boolean hintAccessed = true;
      boolean solAccessed = true;

      result.updateFromFile(runCount, failedCount, regressionCount, hintAccessed, solAccessed);

      String newRunCount = ((Integer) TestUtils.getFieldValue("runCount", result)).toString();
      assertTrue("Run count updated correctly", runCount.equals(newRunCount));

      String newFailedCount = ((Integer) TestUtils.getFieldValue("failedCount", result)).toString();
      assertTrue("Fail count updated correctly", failedCount.equals(newFailedCount));

      String newRegressionCount = ((Integer) TestUtils.getFieldValue("regressionCount", result)).toString();
      assertTrue("Run count updated correctly", regressionCount.equals(newRegressionCount));

      boolean newHintAccessed = ((Boolean) TestUtils.getFieldValue("hintGot", result));
      assertTrue(newHintAccessed);

      boolean newSolAccessed = ((Boolean) TestUtils.getFieldValue("solGot", result));
      assertTrue(newSolAccessed);
   }

   @Test
   public void testSetHintAccessedMethod() {
      result.setHintAccessed();

      boolean newHintAccessed = ((Boolean) TestUtils.getFieldValue("hintGot", result));
      assertTrue(newHintAccessed);
   }

   @Test
   public void testSetSolAccessedMethod() {
      result.setSolutionAccessed();

      boolean newSolAccessed = ((Boolean) TestUtils.getFieldValue("solGot", result));
      assertTrue(newSolAccessed);
   }

   @Test
   public void testGetScoreNoRuns() {
      int expected = 0;

      assertTrue("Get score() returns 0 for 0 runs", result.getScore() == expected);
   }

   @Test
   public void testGetScoreWinNoHintNoSol() {
      double expected = 2;
      result.logTestSuccess();

      assertTrue("getScore() returns 2 for 1 win, No hints & no solution", result.getScore() == expected);
   }

   @Test
   public void testGetScoreWinHintNoSol() {
      double expected = 1.5;
      result.logTestSuccess();
      result.setHintAccessed();

      assertTrue("getScore() returns 1 for 1 win, a hint & no solution", result.getScore() == expected);
   }

   @Test
   public void testGetScoreWinHintSol() {
      double expected = 0.5;
      result.logTestSuccess();
      result.setHintAccessed();
      result.setSolutionAccessed();

      assertTrue("getScore() returns 0.5 for 1 win, a hint & a solution", result.getScore() == expected);
   }

   @Test
   public void testGetScoreWinNoHintSol() {
      double expected = 1;
      result.logTestSuccess();
      result.setSolutionAccessed();

      assertTrue("getScore() returns 1 for 1 win, No hints & a solution", result.getScore() == expected);
   }
}
