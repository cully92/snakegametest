/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package snakegametest.runner.tests;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import snakegametest.runner.Report;
import snakegametest.runner.SnakeTestResult;
import snakegametest.tests.SnakeTest;
import snakegametest.tests.ui.BoardUITest;
import snakegametest.tests.ui.SnakeUITest;

/**
 *
 * @author Sean
 */
public class ReportTest {

   private Report report;
   private static String tempFile = "temp.txt";

   @Before
   public void before() {
      report = new Report("test.report.txt");
   }

   @AfterClass
   public static void afterClass() {
      new File(tempFile).delete();
   }

   /**
    * Test the constructor creates the correct number of results (3)
    */
   @Test
   public void testConstructorCreatesCorrectNumberOfResults() {
      // Should have results for 1.1, 1.42
      List<SnakeTestResult> results11 = report.getResults(1.1);
      List<SnakeTestResult> results142 = report.getResults(1.42);

      assertNotNull(results11);
      assertNotNull(results142);
   }

   /**
    * Test the constructor creates multiple results per task (in the case of
    * multiple tests per task)
    */
   @Test
   public void testConstructorCreatesMultipleResultsPerTask() {
      // The test.report.txt file contains 2 results for 1.42
      List<SnakeTestResult> results11 = report.getResults(1.1);
      List<SnakeTestResult> results142 = report.getResults(1.42);

      assertTrue(results11.size() == 1);
      assertTrue(results142.size() == 2);
   }

   /**
    * Test that the results read from file are correct
    */
   @Test
   public void testConstructorCreatesCorrectResults() {
      List<SnakeTestResult> results11 = report.getResults(1.1);
      List<SnakeTestResult> results142 = report.getResults(1.42);

      String expected = getExpectedReport().get(0) + "\n";
      String actual = results11.get(0).toString();
      assertTrue("Results for 1.1 don't match", expected.equals(actual));

      expected = getExpectedReport().get(1) + "\n";
      actual = results142.get(0).toString();
      assertTrue("Results for 1.42[0] don't match", expected.equals(actual));
    
      expected = getExpectedReport().get(2) + "\n";
      actual = results142.get(1).toString();
      assertTrue("Results for 1.42[1] don't match", expected.equals(actual));
   }

   /**
    * Test the 'getLinesFromFile' method works as expected
    */
   @Test
   public void testGetLinesFromFile() {
      List<String> expected = getExpectedReport();
      
      List<String> actual = Report.getLinesFromFile("test.report.txt");
      assertTrue(expected.equals(actual));
   }

   @Test
   public void testWriteToFileMethod() {
      report.writeToFile(tempFile);

      List<String> expected = getExpectedReport();
      
      List<String> actual = Report.getLinesFromFile(tempFile);
      assertTrue(expected.equals(actual));
   }
   
   @Test
   public void testAddTestResultExistingTask() {
      String methodName = "testRowField";
      double task = 1.42;
      report.addTestResult(task, report.getMethod(task, methodName));

      List<SnakeTestResult> results = report.getResults(task);
      boolean resultFound = false;

      // Try to find the task
      for (SnakeTestResult result : results) {
         if (result.getMethod().getName().equals(methodName))
            resultFound = true;
      }

      assertTrue("Test adding a result to an existing task succeeds", resultFound);
   }

   /** Try to add a test result for a task which already
    * has a test attached
    */
   @Test
   public void testAddTestResultNewTask() {
      String methodName = "testGridCorrectSize";
      report.addTestResult(1.2, report.getMethod(1.2, methodName));

      List<SnakeTestResult> results = report.getResults(1.2);
      boolean resultFound = false;

      // Try to find the task
      for (SnakeTestResult result : results) {
         if (result.getMethod().getName().equals(methodName))
            resultFound = true;
      }

      assertTrue("Test adding a result to an existing task succeeds", resultFound);
   }

   @Test
   public void testWriteToFileWithChangedReport() {
      report.addTestResult(1.2, report.getMethod(1.2, "testGridCorrectSize"));
      report.writeToFile(tempFile);

      List<String> expected = getExpectedReport();
      expected.add("Exercise 1.2	Run: 0	Succeeded: 0	Failed: 0	Regressed: 0	H0S0	testGridCorrectSize");
      Collections.sort(expected);

      List<String> actual = Report.getLinesFromFile(tempFile);
      Collections.sort(actual);
      
      assertTrue(expected.equals(actual));
   }

   /**
    * Solution should be available after 3 fails
    */
   @Test
   public void testSolutionAvailableMethod() {
      assertTrue("Solution available with 3 failures", report.isSolutionAvailable(1.1));
      assertFalse("Solution not available with 0 failures", report.isSolutionAvailable(1.42));
   }

   @Test
   public void testCalculateScoreMethod() {
      double score = report.calculateScore();
      //Score starts off at 2, remove 0.5 if hint accessed and 1 if solution accessed
      double expected = 4.5;

      assertTrue("Score calculated correctly", expected == score);
   }

   @Test
   public void testResultExistsWithExistingMethod() {
      double task = 1.1;
      assertTrue("Result exists for testTitleChanged", report.resultExists(task, report.getMethod(task, "testTitleChanged")));
   }

   @Test(expected=RuntimeException.class)
   public void testResultExistsWithMissingMethod() {
      double task = 1.1;
      report.resultExists(task, report.getMethod(task, "imNotAMethod"));
   }

   @Test
   public void testGetMethodWithExistingMethod() {
      double task = 1.1;
      Method method = report.getMethod(task, "testTitleChanged");
      Class<?> testClass = method.getDeclaringClass();

      boolean result = testClass.equals(SnakeUITest.class);
      assertTrue("getMethod() gets the correct method for 'testTitleChanged'", result);


   }

   @Test(expected=RuntimeException.class)
   public void testGetMethodWithMissingMethod() {
      double task = 1.1;
      report.getMethod(task, "iDontExist");
   }

   @Test
   public void testGetMethodReturnsCorrectMethodWithAmbiguousName() {
      Method getBoardFromBoardUI = report.getMethod(1.44, "testBoardField");
      Method getBoardFromSnake = report.getMethod(1.71, "testBoardField");

      boolean result = getBoardFromBoardUI.getDeclaringClass().equals(BoardUITest.class);
      assertTrue("getMethod() retrieved correct testBoardField from 1.44", result);

      result = getBoardFromSnake.getDeclaringClass().equals(SnakeTest.class);
      assertTrue("getMethod() retrieved correct testBoardField from 1.71", result);
   }

   /**
    * Get the expected report Strings (from test.report.txt)
    * @return
    */
   private List<String> getExpectedReport() {
      List<String> expected = new ArrayList<String>();
      expected.add("Exercise 1.1	Run: 5	Succeeded: 2	Failed: 3	Regressed: 0	H1S0	testTitleChanged");
      expected.add("Exercise 1.42	Run: 1	Succeeded: 1	Failed: 0	Regressed: 0	H0S1	testSideSizeField");
      expected.add("Exercise 1.42	Run: 1	Succeeded: 1	Failed: 0	Regressed: 0	H0S0	testRowField");
      return expected;
   }
}

