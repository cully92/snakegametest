package snakegametest.runner.tests;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import org.junit.AfterClass;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.notification.Failure;
import snakegametest.runner.ProgressTracker;
import snakegametest.runner.ProgressTracker.ResultCollector;

/**
 *
 * @author Sean
 */
public class ProgressTrackerTest {

   private static File progressFile;
   private static File tempProgressFile;

   @BeforeClass
   public static void beforeClass() throws IOException {
      progressFile = new File("progress.txt");

      tempProgressFile = new File("progress.old.txt");
      progressFile.renameTo(tempProgressFile);

      progressFile.createNewFile();
   }

   @AfterClass
   public static void afterClass() {
      progressFile.delete();

      tempProgressFile.renameTo(progressFile);
   }

   @Test
   public void testGetNextTask() {
      List<String> lines = new ArrayList<String>();
      lines.add("1.42");
      writeLinesToFile(progressFile, lines);

      assertTrue(ProgressTracker.getNextTask() == 1.44);
   }

   @Test
   public void testSetLastTaskCompleted() {
      ProgressTracker.setLastTaskCompleted(2.3);

      assertTrue(ProgressTracker.getNextTask() == 2.4);
   }

   @Test
   public void testResultCollectorAddTask() {
      ResultCollector.addTask(1.1);

      List<Double> expected = new ArrayList<Double>();
      expected.add(1.1);

      assertTrue(expected.equals(ResultCollector.getTasks()));
      
   }

   @Test
   public void testResultCollectorAddFailure() {
      ResultCollector.addFailure(null);

      List<Failure> expected = new ArrayList<Failure>();
      expected.add(null);

      assertTrue(expected.equals(ResultCollector.getFailures()));
   }

   @Test
   public void testResultCollectorReset() {
      ResultCollector.addTask(1.1);
      ResultCollector.addFailure(null);

      ResultCollector.reset();

      assertTrue(ResultCollector.getFailures().isEmpty());
      assertTrue(ResultCollector.getTasks().isEmpty());
   }

   private void writeLinesToFile(File file, List<String> lines) {
      BufferedWriter writer = null;
      try {

         writer = new BufferedWriter(new FileWriter(file));

         for (String line : lines) {
            writer.write(line);
         }

      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try {
            writer.close();
         } catch (IOException e) {}
      }
   }

   private double getLastQFromFile() {
      BufferedReader reader = null;
      try {
         reader = new BufferedReader(new FileReader("progress.txt"));
         String line = reader.readLine();
         return Double.parseDouble(line);
      } catch (FileNotFoundException e) {
         // Valid, if tester not run before
      } catch (IOException e) {
         e.printStackTrace();
      } finally {
         try {
            if (reader != null) {
               reader.close();
            }
         } catch (IOException e) {
         }
      }

      return 0;
   }
}
