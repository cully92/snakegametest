package snakegametest.runner.tests;

import java.util.List;
import org.junit.Test;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.TestClass;
import snakegametest.runner.ProgressTracker;
import snakegametest.runner.Task;
import static org.junit.Assert.assertArrayEquals;

/**
 * General / misc tests for Snake tester
 */
public class RunnerTest {

   /**
    * Test that all test methods in the test classes also have a Task annotation
    */
   @Test
   public void testAllTestMethodsHaveTaskAnnotation() {
      Class<?>[] testClasses = ProgressTracker.getTestClasses();

      for (Class<?> testClass : testClasses) {
         TestClass tClass = new TestClass(testClass);
         List<FrameworkMethod> testMethods = tClass.getAnnotatedMethods(Test.class);
         List<FrameworkMethod> taskMethods = tClass.getAnnotatedMethods(Task.class);
         assertArrayEquals(testMethods.toArray(new FrameworkMethod[0]), taskMethods.toArray(new FrameworkMethod[0]));
      }
   }
}
