package snakegametest.runner;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation that takes a value describing which worksheet it comes from,
 * and then the exercise number.
 *
 * E.g for the 1st Exercise in the 1st worksheet:
 * "@Task(1.1)"
 * 
 * @author Sean
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Task {

	/**
	 * @return the value of the Task annotation
	 */
	double value();
}
