package snakegametest.hint;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Reads hints and solutions from their respective directories.
 */
public class HintReader {

   private final static String NO_HINT = "Hint not found";
   private final static String NO_SOL = "Solution not found";

   /**
    * Return the hint (as a String that includes newline and tab characters) for a given task number
    * @param task
    * @return
    */
   public static String getHint(double task) {

      // Hints for each task are stored
      InputStream inputStream = HintReader.class.getResourceAsStream("/hints/" + task + ".txt");
      if (inputStream == null) {
         return NO_HINT;
      }

      BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
      return getHintMessage(input);
   }

   /**
    * Return the SOLUTION (as a String that includes newline and tab characters) for a given task number
    * @param task
    * @return
    */
   public static String getSolution(double task) {

      // Hints for each task are stored
      InputStream inputStream = HintReader.class.getResourceAsStream("/sols/" + task + ".txt");
      if (inputStream == null) {
         return NO_SOL;
      }

      BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
      return getHintMessage(input);

   }

   /**
    * Helper method to read the message from a file
    * @param reader
    * @return
    */
   private static String getHintMessage(BufferedReader reader) {
      try {
         String line, hintMsg = "";
         while ((line = reader.readLine()) != null) {
            hintMsg += line + "\n";
         }
         return hintMsg;
      } catch (IOException ex) {
         return NO_HINT;
      }
   }
}