package snakegametest.hint;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.junit.runner.Description;
import org.junit.runner.notification.Failure;
import snakegametest.runner.ProgressTracker.ResultCollector;
import snakegametest.runner.Report;
import snakegametest.runner.Task;

/**
 * UI Component for the Hint and solution functionality of the Snake Tester
 */
public class HintUI extends JFrame {

   private static final long serialVersionUID = 1L;
   
   private JList listBox;
   private JTextArea hintArea;
   private FailureAdapter[] failureAdapters;
   private Report report = new Report();
   private JButton getSolButton;

   public HintUI() {
      
      setupJFrame();
      displayResults();
   }

   /**
    * Setup the main UI window (JFrame)
    */
   private void setupJFrame() {
      setTitle("Snake Game Tester: Hints");
      setSize(450, 300);
      setResizable(true);

      // This tells the program to exit when the red 'X' in the top right
      // corner is clicked
      setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

      setResizable(true);
      setFocusable(true);
   }

   /**
    * Display the results of the test run
    */
   private void displayResults() {
      addLabelPanel();
      addBottomPanel();

      // Ensure the window looks correct
      pack();
   }

   /**
    * Add text to the top panel
    */
   private void addLabelPanel() {
      JPanel topPanel = new JPanel();
      topPanel.setLayout(new BorderLayout());
      getContentPane().add(topPanel, BorderLayout.NORTH);

      JLabel label = new JLabel("   Failed tests:");
      topPanel.add(label, BorderLayout.WEST);
      topPanel.add(new JLabel("Hint:   "), BorderLayout.EAST);
   }

   /**
    * Add the list of failed tests and the hint area
    */
   private void addBottomPanel() {
      JPanel bottomPanel = new JPanel(new BorderLayout(20, 10));
      bottomPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
      getContentPane().add(bottomPanel, BorderLayout.CENTER);

      setupTestFailureListBox(bottomPanel);

      JPanel hintPanel = new JPanel(new BorderLayout(0, 20));
      bottomPanel.add(hintPanel, BorderLayout.EAST);

      hintArea = new JTextArea(8, 60);
      // Set the text to the initial failure message
      hintArea.setText("Failure message: " + failureAdapters[0].getFailureMessage());
      hintArea.setEditable(false);
      hintPanel.add(new JScrollPane(hintArea), BorderLayout.CENTER);

      JPanel btnPanel = new JPanel(new BorderLayout(0, 20));
      hintPanel.add(btnPanel, BorderLayout.SOUTH);



      JButton hintBtn = new JButton("Get Hint");
      btnPanel.add(hintBtn, BorderLayout.WEST);

      hintBtn.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            FailureAdapter adapter = (FailureAdapter) listBox.getSelectedValue();
            String failureHint = HintReader.getHint(adapter.getTaskNumber());
            hintArea.setText(failureHint);

            report.setHintAccessed(adapter.getTaskNumber());
         }
      });

      getSolButton = new JButton("Get Solution");
      double taskNo = failureAdapters[0].getTaskNumber();
      getSolButton.setEnabled(report.isSolutionAvailable(taskNo));
      btnPanel.add(getSolButton, BorderLayout.EAST);

      getSolButton.addActionListener(new ActionListener() {

         @Override
         public void actionPerformed(ActionEvent e) {
            FailureAdapter adapter = (FailureAdapter) listBox.getSelectedValue();
            String taskSolution = HintReader.getSolution(adapter.getTaskNumber());
            hintArea.setText(taskSolution);

            report.setSolutionAccessed(adapter.getTaskNumber());
         }
      });
   }

   /**
    * Set up the listbox showing test failures, and add a selection listener
    * so that we can initially display the error message
    */
   private void setupTestFailureListBox(JPanel parent) {
      List<Failure> failures = ResultCollector.getFailures();
      failureAdapters = new FailureAdapter[failures.size()];

      // Get an array of all the failed texts
      for (int i = 0; i < failures.size(); i++) {
         Failure failure = failures.get(i);
         Description description = failure.getDescription();
         Task annotation = description.getAnnotation(Task.class);
         String friendlyName = "Task: " + annotation.value() + ", " + description.getMethodName();
         failureAdapters[i] = new FailureAdapter(failure, friendlyName);
      }

      Arrays.sort(failureAdapters);
      listBox = new JList(failureAdapters);
      listBox.setSelectedIndex(0);
      listBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      parent.add(new JScrollPane(listBox), BorderLayout.WEST);

      listBox.addListSelectionListener(new ListSelectionListener() {

         @Override
         public void valueChanged(ListSelectionEvent e) {
            JList source = (JList) e.getSource();
            FailureAdapter failure = (FailureAdapter) source.getSelectedValue();

            hintArea.setText("Failure message: " + failure.getFailureMessage());
            getSolButton.setEnabled(report.isSolutionAvailable(failure.getTaskNumber()));
         }
      });
   }

   /**
    * Class to adapt failures to a more human-friendly representation, while
    * still retaining the extra information needed from the Failure object
    */
   class FailureAdapter implements Comparable<FailureAdapter> {

      private final String name;
      private final Failure failure;

      public FailureAdapter(Failure failure, String humanFriendlyName) {
         this.failure = failure;
         this.name = humanFriendlyName;
      }

      public String getFailureMessage() {
         return failure.getMessage();
      }

      @Override
      public String toString() {
         return name;
      }

      @Override
      public int compareTo(FailureAdapter other) {

         // Want to extract task number
         double thisTask = getTaskNumber();
         double otherTask = other.getTaskNumber();

         // Sort by the task number
         return (int) (thisTask - otherTask);
      }

      /**
       * Extract the task number from the FailureAdapter object, by using the
       * toString() method
       *
       * @param adapter
       * @return task number
       */
      public double getTaskNumber() {
         // Original string of form Task X.X, testName
         String[] parts = toString().split(", ");
         // The task starts 6 characters in
         return Double.parseDouble(parts[0].substring(6));
      }
   }
}
