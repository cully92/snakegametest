The constructor method ALWAYS has the same name as the class, so look in the method called Board.

In this method, you want to put the following line, telling Java how many rows and columns your grid will have:

this.grid = new Cell[rows][cols]
